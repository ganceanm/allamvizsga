#!/bin/bash
# Build script

#Path variables
DIR="/var/www/allamvizsga"
NGINX_PATH="/var/www/html"
JBOSS_PATH="/opt/wildfly/standalone/deployments"

#Front-end build
front_end() {
	cd $DIR/front-end/allamvizsga-front-end
	
	npm i
	npm run build
	FSTATUS=$?
	echo "-------------------------------------------------------------------------------------------"
	if [ $FSTATUS -eq 0 ]; then
		echo "Front-end build successful..."
		cp -a $DIR/front-end/allamvizsga-front-end/build/. $NGINX_PATH
	else
		echo "Front-end build failed with code $FSTATUS"
	fi
	echo "-------------------------------------------------------------------------------------------"
	return $FSTATUS
}

#Back-end build
back_end() {
	cd $DIR/back-end

	echo "Starting back-end build..."

	mvn clean install
	BSTATUS=$?
	echo "-------------------------------------------------------------------------------------------"
	if [ $BSTATUS -eq 0 ]; then
		echo "Deployment Successful"
		rm $JBOSS_PATH/back-end.war
		cp $DIR/back-end/target/back-end-1.0-SNAPSHOT.war $JBOSS_PATH/back-end.war
		echo "Wait for WildFly to re-deploy..."
	else
		echo "Deployment Failed with status code $BSTATUS"
	fi
	echo "-------------------------------------------------------------------------------------------"
	return $BSTATUS
}

###
#Main script starts here
###

echo "Starting build..."

cd $DIR
git pull

FRONT_RES="build not run"
BACK_RES="build not run"
case $1 in
	front-end)
		front_end
		FRONT_RES=$?
		;;
	back-end)
		back_end
		BACK_RES=$?
		;;
	full)
		front_end
		FRONT_RES=$?
		back_end
		BACK_RES=$?
		;;
	*)
		front_end
		FRONT_RES=$?
		echo ""
		read -p "Do you want to proceed to the back-end build?[y/n]: " CONTINUE

		if [ "$CONTINUE" = "y" ]; then
			back_end
			BACK_RES=$?
		fi
		;;
esac

echo ""
echo "====================================== FINAL RESULTS ======================================"
echo ""
echo "FRONT-END: $FRONT_RES"
echo "BACK_END:  $BACK_RES"
echo ""
echo "==========================================================================================="
echo ""

