package com.ganceanm.allamvizsga.services.email;

public interface EmailService {
	public void sendEmail(String mailTo, String subject, String text);
}
