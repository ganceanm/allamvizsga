package com.ganceanm.allamvizsga.room.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.aux.exception.UserNotFoundException;
import com.ganceanm.allamvizsga.room.api.message.RoomDetailsMsg;
import com.ganceanm.allamvizsga.room.model.Room;
import com.ganceanm.allamvizsga.user.api.converter.SimpleUserConverter;
import com.ganceanm.allamvizsga.user.api.message.SimpleUserMsg;
import com.ganceanm.allamvizsga.user.model.User;
import com.ganceanm.allamvizsga.user.service.UserService;

@Component
public class RoomDetailsConverter {
	
	@Autowired
	SimpleUserConverter userConverter;
	
	@Autowired
	UserService usersService;
	
	public RoomDetailsMsg toMsg(Room from) {
		RoomDetailsMsg to = new RoomDetailsMsg();
		
		List<User> userList = new ArrayList<>();
		List<SimpleUserMsg> msgList = new ArrayList<>();
		
		try {
			userList = usersService.getByRoom(from);
		}catch (UserNotFoundException e) {
			userList.clear();
		}
		
		if(!userList.isEmpty()) {
			for(User u : userList) {
				msgList.add(userConverter.toMsg(u));
			}
		}
		
		to.setId(from.getId());
		to.setRoomNumber(from.getRoomNumber());
		to.setRoomType(from.getRoomType());
		to.setRoomStatus(from.getRoomStatus());
		to.setUserList(msgList);
		
		return to;
	}
}
