package com.ganceanm.allamvizsga.room.model;

public enum RoomStatus {
	Active,
	Inactive
}
