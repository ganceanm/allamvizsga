package com.ganceanm.allamvizsga.payment.api.message;

import java.util.Date;
import java.util.List;

import com.ganceanm.allamvizsga.payment.model.PaymentType;

public class FindPaymentMsg {
	private List<String> searchParams;
	private Date fromDate;
	private Date toDate;
	private PaymentType type;
	private Long page;

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public List<String> getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(List<String> searchParams) {
		this.searchParams = searchParams;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}

}
