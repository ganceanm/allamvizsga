package com.ganceanm.allamvizsga.payment.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.ganceanm.allamvizsga.search.SearchCriteria;

public class PaymentSpecification implements Specification<Payment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3305982466343454531L;
	private SearchCriteria criteria;

	public PaymentSpecification(SearchCriteria criteria) {
		super();
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<Payment> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		if (criteria.getOperation().equalsIgnoreCase(":")) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
			final List<Predicate> filters = new ArrayList<Predicate>();
			for(String key : criteria.getKey()) {
				for(String value : criteria.getValue()) {
					predicates.add(builder.like(root.get("user").<String> get(key), value + "%"));
				}
			}
			Predicate namePredicate = builder.or(predicates.toArray(new Predicate[predicates.size()]));
			
			
			
			if(criteria.getOptionals().isPresent()) {
				HashMap<String, ?> optionals = criteria.getOptionals().get();
				
				if(optionals.containsKey("from") || optionals.containsKey("to")) {
					Date from, to;
					if(optionals.containsKey("from")) {
						from = new Date(Long.valueOf((String)optionals.get("from")));
					} else {
						from = new Date(0);
					}
					
					if(optionals.containsKey("from")) {
						to = new Date(Long.valueOf((String)optionals.get("to")));
					} else {
						to = new Date();
					}
					
					Predicate datePredicate = builder.and(
							builder.greaterThanOrEqualTo(root.get("createdAt"), from),
							builder.lessThanOrEqualTo(root.get("createdAt"), to)
							);
					filters.add(datePredicate);
				}
				
				if(optionals.containsValue("filter")) {
					try {
						Predicate typePredicate = builder.equal(root.get("type"), PaymentType.valueOf((String)optionals.get("filter")));
						filters.add(typePredicate);
					} catch (Exception e) {
						
					}
				}
				
				
				
			}
			
			
			
			Predicate filterPredicate = builder.and(filters.toArray(new Predicate[filters.size()]));
			
			return builder.and(namePredicate, filterPredicate);
		}
		return null;
	}

}