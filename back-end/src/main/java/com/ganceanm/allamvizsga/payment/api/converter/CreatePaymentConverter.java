package com.ganceanm.allamvizsga.payment.api.converter;

import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.payment.api.message.CreatePaymentMsg;
import com.ganceanm.allamvizsga.payment.model.Payment;

@Component
public class CreatePaymentConverter {
	public Payment toEntity(CreatePaymentMsg from) {
		Payment to = new Payment();
		
		to.setValue(from.getValue());
		to.setType(from.getType());
		to.setReceiptNr(from.getReceiptNr());
		to.setMonth(from.getMonth());
		
		return to;
	}
}
