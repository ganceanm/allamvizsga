package com.ganceanm.allamvizsga.payment.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ganceanm.allamvizsga.user.model.User;


public interface PaymentRepository extends JpaRepository<Payment, Long>{
	@Query("SELECT p from Payment p where p.user = :user and p.month = :month and month(p.createdAt) >= :timestampMonth and p.deleted = 'false'")
	public Payment findByUserAndMonth(@Param("user") User user, @Param("month") Long month, @Param("timestampMonth") int i);
}
