package com.ganceanm.allamvizsga.payment.api;

import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ganceanm.allamvizsga.payment.api.converter.CreatePaymentConverter;
import com.ganceanm.allamvizsga.payment.api.message.CreatePaymentMsg;
import com.ganceanm.allamvizsga.payment.service.PaymentService;
import com.ganceanm.allamvizsga.search.SearchResponse;
import com.ganceanm.allamvizsga.search.SearchResponseConverter;
import com.ganceanm.allamvizsga.security.clearance.ClearanceOne;

@RestController
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@Autowired
	CreatePaymentConverter createPaymentConverter;

	@Autowired
	SearchResponseConverter searchResponseConverter;

	@PostMapping
	public ResponseEntity<HttpStatus> createPayment(@RequestBody CreatePaymentMsg msg) {
		return paymentService.createPayment(createPaymentConverter.toEntity(msg), msg.getUser());
	}

	@ClearanceOne
	@GetMapping
	public SearchResponse find(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "15") int limit,
			@RequestParam(defaultValue = "") String keyword, // name of the user who paid
			@RequestParam Optional<String> from, @RequestParam Optional<String> to,
			@RequestParam Optional<String> orderby, @RequestParam Optional<String> dir,
			@RequestParam Optional<String> filter) {
		HashMap<String, String> optionals = new HashMap<>();

		if (from.isPresent()) {
			optionals.put("from", from.get());
		}
		if (to.isPresent()) {
			optionals.put("to", to.get());
		}
		if (orderby.isPresent()) {
			optionals.put("orderby", orderby.get());
		}
		if (dir.isPresent()) {
			optionals.put("dir", dir.get());
		}
		if (filter.isPresent()) {
			optionals.put("filter", filter.get());
		}

		return searchResponseConverter.toMsg(paymentService.find(page, limit, keyword, Optional.ofNullable(optionals)));
	}
}
