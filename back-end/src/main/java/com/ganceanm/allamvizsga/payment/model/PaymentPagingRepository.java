package com.ganceanm.allamvizsga.payment.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ganceanm.allamvizsga.user.model.User;



public interface PaymentPagingRepository extends PagingAndSortingRepository<Payment, Long>, JpaSpecificationExecutor<Payment>{
	@Query("SELECT p FROM Payment p where p.user = :user order by p.createdAt desc") 
    Page<Payment> findByUser(@Param("user") User user, Pageable page);
}
