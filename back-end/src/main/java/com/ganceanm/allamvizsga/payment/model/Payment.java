package com.ganceanm.allamvizsga.payment.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ganceanm.allamvizsga.aux.model.BaseTableEntity;
import com.ganceanm.allamvizsga.user.model.User;

@Entity
@Table(name = "payments")
public class Payment extends BaseTableEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@NotNull
	@Enumerated(EnumType.STRING)
	private PaymentType type;

	@NotNull
	private BigDecimal value = new BigDecimal(0);

	@NotNull
	private Long receiptNr;

	@NotNull
	private Long month;

	@Column
	private String paidRoom;

	public String getPaidRoom() {
		return paidRoom;
	}

	public void setPaidRoom(String paidRoom) {
		this.paidRoom = paidRoom;
	}

	public Long getMonth() {
		return month;
	}

	public void setMonth(Long month) {
		this.month = month;
	}

	public Long getReceiptNr() {
		return receiptNr;
	}

	public void setReceiptNr(Long receiptNr) {
		this.receiptNr = receiptNr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}
	
	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
