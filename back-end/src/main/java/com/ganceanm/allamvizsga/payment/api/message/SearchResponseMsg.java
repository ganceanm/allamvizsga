package com.ganceanm.allamvizsga.payment.api.message;

import java.util.List;

public class SearchResponseMsg {
	private List<SimplePaymentMsg> payments;
	private Boolean hasMore;
	private Long pageNr;

	public List<SimplePaymentMsg> getPayments() {
		return payments;
	}

	public void setPayments(List<SimplePaymentMsg> payments) {
		this.payments = payments;
	}

	public Boolean getHasMore() {
		return hasMore;
	}

	public void setHasMore(Boolean hasMore) {
		this.hasMore = hasMore;
	}

	public Long getPageNr() {
		return pageNr;
	}

	public void setPageNr(Long pageNr) {
		this.pageNr = pageNr;
	}

}
