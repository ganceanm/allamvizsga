package com.ganceanm.allamvizsga.payment.api.message;

import java.math.BigDecimal;
import java.util.Date;

import com.ganceanm.allamvizsga.payment.model.PaymentType;
import com.ganceanm.allamvizsga.user.api.message.SimpleUserMsg;

public class SimplePaymentMsg {
	private Long id;
	private SimpleUserMsg user;
	private Date timestamp;
	private BigDecimal value;
	private PaymentType type;
	private Long receiptNr;
	private Long month;
	private String paidRoom;

	public String getPaidRoom() {
		return paidRoom;
	}

	public void setPaidRoom(String paidRoom) {
		this.paidRoom = paidRoom;
	}

	public Long getMonth() {
		return month;
	}

	public void setMonth(Long month) {
		this.month = month;
	}

	public Long getReceiptNr() {
		return receiptNr;
	}

	public void setReceiptNr(Long receiptNr) {
		this.receiptNr = receiptNr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SimpleUserMsg getUser() {
		return user;
	}

	public void setUser(SimpleUserMsg user) {
		this.user = user;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}

}
