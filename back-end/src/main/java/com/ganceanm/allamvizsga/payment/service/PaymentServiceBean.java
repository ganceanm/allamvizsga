package com.ganceanm.allamvizsga.payment.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ganceanm.allamvizsga.payment.model.Payment;
import com.ganceanm.allamvizsga.payment.model.PaymentPagingRepository;
import com.ganceanm.allamvizsga.payment.model.PaymentRepository;
import com.ganceanm.allamvizsga.payment.model.PaymentSpecification;
import com.ganceanm.allamvizsga.payment.model.PaymentType;
import com.ganceanm.allamvizsga.search.SearchCriteria;
import com.ganceanm.allamvizsga.user.model.User;
import com.ganceanm.allamvizsga.user.service.UserService;

@Service
public class PaymentServiceBean implements PaymentService {

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	PaymentPagingRepository paymentPagingRepository;

	@Autowired
	UserService usersService;

	@Override
	public Payment save(Payment payment) {
		if (payment.getId() == null) {
			payment.setCreatedAt(new Date());
		}

		payment.setModifiedAt(new Date());

		return paymentRepository.save(payment);
	}

	@Override
	public Payment getById(Long paymentId) {
		return paymentRepository.getOne(paymentId);
	}

	@Override
	public Page<Payment> findByUser(User users, Long page) {
		return paymentPagingRepository.findByUser(users, PageRequest.of(page.intValue(), 15));
	}

	@Override
	public ResponseEntity<HttpStatus> createPayment(Payment payment, Long userId) {
		User user = usersService.getById(userId);

		if (user == null) {
			return ResponseEntity.notFound().build();
		}

		if (payment.getType() == PaymentType.Room) {

			payment.setPaidRoom(user.getRoom().getRoomNumber());
			int timestampMonth = 0;
			Calendar calendar = Calendar.getInstance();
			int currentMonth = calendar.get(Calendar.MONTH);
			if (currentMonth > 8)
				timestampMonth = 8;

			if (paymentRepository.findByUserAndMonth(user, payment.getMonth(), timestampMonth) != null) {
				return ResponseEntity.status(HttpStatus.CONFLICT).build();
			}
		}

		payment.setUser(user);
		save(payment);
		return ResponseEntity.accepted().build();
	}

	@Override
	public Page<Payment> find(int page, int limit, String keyword, Optional<HashMap<String, ?>> optionals) {
		List<String> keys = new ArrayList<>();
		keys.add("lastName");
		keys.add("firstName");

		List<String> text = Arrays.asList(keyword.split(" "));

		PaymentSpecification spec = new PaymentSpecification(new SearchCriteria(keys, ":", text, optionals));

		return paymentPagingRepository.findAll(spec, PageRequest.of(page, limit));
	}
}
