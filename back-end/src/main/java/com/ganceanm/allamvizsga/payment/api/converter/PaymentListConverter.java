package com.ganceanm.allamvizsga.payment.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.payment.api.message.SimplePaymentMsg;
import com.ganceanm.allamvizsga.payment.model.Payment;
import com.ganceanm.allamvizsga.user.api.converter.SimpleUserConverter;

@Component
public class PaymentListConverter {
	
	@Autowired
	SimpleUserConverter userConverter;
	
	public List<SimplePaymentMsg> toMsg(List<Payment> from) {
		List<SimplePaymentMsg> to = new ArrayList<>();
		
		
		for(Payment p : from) {
			SimplePaymentMsg msg = new SimplePaymentMsg();
			msg.setId(p.getId());
			msg.setUser(userConverter.toMsg(p.getUser()));
			msg.setValue(p.getValue());
			msg.setType(p.getType());
			msg.setTimestamp(p.getCreatedAt());
			msg.setReceiptNr(p.getReceiptNr());
			msg.setMonth(p.getMonth());
			msg.setPaidRoom(p.getPaidRoom());
			to.add(msg);
		}
		return to;
	}
}
