package com.ganceanm.allamvizsga.payment.service;

import java.util.HashMap;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ganceanm.allamvizsga.payment.model.Payment;
import com.ganceanm.allamvizsga.user.model.User;

public interface PaymentService {
	public Payment save(Payment payment);
	public Payment getById(Long paymentId);
	public ResponseEntity<HttpStatus> createPayment(Payment payment, Long userId);
	public Page<Payment> findByUser(User users, Long page);
	public Page<Payment> find(int page, int limit, String keyword, Optional<HashMap<String, ?>> optionals);
}
