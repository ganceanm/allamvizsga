package com.ganceanm.allamvizsga.payment.api.message;

import java.math.BigDecimal;

import com.ganceanm.allamvizsga.payment.model.PaymentType;

public class CreatePaymentMsg {
	private Long user;
	private PaymentType type;
	private BigDecimal value;
	private Long receiptNr;
	private Long month;

	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public Long getMonth() {
		return month;
	}

	public void setMonth(Long month) {
		this.month = month;
	}

	public Long getReceiptNr() {
		return receiptNr;
	}

	public void setReceiptNr(Long receiptNr) {
		this.receiptNr = receiptNr;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
