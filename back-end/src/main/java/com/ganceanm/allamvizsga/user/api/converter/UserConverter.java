package com.ganceanm.allamvizsga.user.api.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.aux.exception.UserNotFoundException;
import com.ganceanm.allamvizsga.user.api.message.UserMsg;
import com.ganceanm.allamvizsga.user.model.User;
import com.ganceanm.allamvizsga.user.service.UserService;

@Component
public class UserConverter {
	@Autowired
	private UserService userService;
	
	public UserMsg toMsg(User from) {
		UserMsg to = new UserMsg();
		
		to.setId(from.getId());
		to.setUserName(from.getUserName());
		to.setRoom(from.getRoom());
		to.setStatus(from.getStatus());
		to.setUserRole(from.getUserRole());
		to.setFirstName(from.getFirstName());
		to.setLastName(from.getLastName());
		to.setStudentClass(from.getStudentClass());
		to.setStudentYear(from.getStudentYear());
		to.setPhoneNumber(from.getPhoneNumber());
		
		return to;
	}
	
	public User toEntity(UserMsg from) throws UserNotFoundException{
		User to = userService.getById(from.getId());
		
		if(to.getId() == null) {
			throw new UserNotFoundException();
		} else {
			to.setFirstName(from.getFirstName());
			to.setLastName(from.getLastName());
			to.setUserName(from.getUserName());
			to.setPhoneNumber(from.getPhoneNumber());
			to.setStudentClass(from.getStudentClass());
			to.setStudentYear(from.getStudentYear());
		}
		
		return to;
	}
}
