package com.ganceanm.allamvizsga.user.api.message;

import com.ganceanm.allamvizsga.user.model.StudentClass;
import com.ganceanm.allamvizsga.user.model.StudentYear;
import com.ganceanm.allamvizsga.user.model.UserRole;
import com.ganceanm.allamvizsga.user.model.UserStatus;

public class UserSearchMsg {
	private Long userId;
	private String userName;
	private String firstName;
	private String lastName;
	private StudentClass studentClass;
	private StudentYear studentYear;
	private String roomNumber;
	private UserRole userRole;
	private UserStatus status;

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public StudentClass getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(StudentClass studentClass) {
		this.studentClass = studentClass;
	}

	public StudentYear getStudentYear() {
		return studentYear;
	}

	public void setStudentYear(StudentYear studentYear) {
		this.studentYear = studentYear;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

}
