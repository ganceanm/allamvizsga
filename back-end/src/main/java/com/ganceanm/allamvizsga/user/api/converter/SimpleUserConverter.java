package com.ganceanm.allamvizsga.user.api.converter;

import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.user.api.message.SimpleUserMsg;
import com.ganceanm.allamvizsga.user.model.User;

@Component
public class SimpleUserConverter {
	public SimpleUserMsg toMsg(User from) {
		SimpleUserMsg to = new SimpleUserMsg();
		
		to.setUserId(from.getId());
		
		if(from.getRoom() != null) {
			to.setRoomNumber(from.getRoom().getRoomNumber());	
		}
		
		to.setFirstName(from.getFirstName());
		
		to.setLastName(from.getLastName());
		
		to.setStudentClass(from.getStudentClass());
		
		to.setStudentYear(from.getStudentYear());
		
		return to;
	}
}
