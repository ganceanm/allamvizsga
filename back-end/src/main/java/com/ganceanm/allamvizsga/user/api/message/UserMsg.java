package com.ganceanm.allamvizsga.user.api.message;

import java.math.BigDecimal;

import com.ganceanm.allamvizsga.room.model.Room;
import com.ganceanm.allamvizsga.user.model.StudentClass;
import com.ganceanm.allamvizsga.user.model.StudentYear;
import com.ganceanm.allamvizsga.user.model.UserRole;
import com.ganceanm.allamvizsga.user.model.UserStatus;

public class UserMsg {
	private Long id;

	private String userName;

	private UserRole userRole;

	private Room room;

	private UserStatus status;
	
	private String firstName;
	
	private String lastName;
	
	private StudentClass studentClass;
	
	private StudentYear studentYear;

	private String phoneNumber;
	
	
	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public StudentClass getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(StudentClass studentClass) {
		this.studentClass = studentClass;
	}

	public StudentYear getStudentYear() {
		return studentYear;
	}

	public void setStudentYear(StudentYear studentYear) {
		this.studentYear = studentYear;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

}
