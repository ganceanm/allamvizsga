package com.ganceanm.allamvizsga.user.model;

public enum StudentClass {
	GI(StudentYear.III),
	AK(StudentYear.III),
	KGI(StudentYear.III),
	MARK(StudentYear.III),
	GS(StudentYear.III),
	MM(StudentYear.III),
	EM(StudentYear.III),
	KM(StudentYear.III),
	PR(StudentYear.III),
	ROA(StudentYear.III),
	VOA(StudentYear.III);
	
	private StudentYear maxYear;
	
	private StudentClass(StudentYear year) {
		this.maxYear = year;
	}
	
	public StudentYear getMaxYear() {
		return this.maxYear;
	}
}
