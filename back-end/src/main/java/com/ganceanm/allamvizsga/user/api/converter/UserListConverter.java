package com.ganceanm.allamvizsga.user.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.user.api.message.UserSearchMsg;
import com.ganceanm.allamvizsga.user.model.User;

@Component
public class UserListConverter {

	public List<UserSearchMsg> toMsg(List<User> from) {
	    
		List<UserSearchMsg> to = new ArrayList<>();

		for (User u : from) {
			UserSearchMsg usMsg = new UserSearchMsg();
			usMsg.setUserId(u.getId());
			usMsg.setUserName(u.getUserName());
			usMsg.setFirstName(u.getFirstName());
			usMsg.setLastName(u.getLastName());
			if (u.getRoom() != null) {
				usMsg.setRoomNumber(u.getRoom().getRoomNumber());
			}
			usMsg.setUserRole(u.getUserRole());
			usMsg.setStudentClass(u.getStudentClass());
			usMsg.setStudentYear(u.getStudentYear());
			usMsg.setStatus(u.getStatus());
			to.add(usMsg);
		}


		return to;
	}
}
