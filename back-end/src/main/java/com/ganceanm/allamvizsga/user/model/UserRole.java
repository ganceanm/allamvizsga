package com.ganceanm.allamvizsga.user.model;

public enum UserRole {
	SYS_ADMIN,
	ADMIN,
	ECONOMIC,
	ROOM_ADMIN,
	STUDENT,
}
