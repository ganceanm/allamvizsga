package com.ganceanm.allamvizsga.user.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ganceanm.allamvizsga.aux.exception.NotUniqueUserNameException;
import com.ganceanm.allamvizsga.aux.exception.UserNotFoundException;
import com.ganceanm.allamvizsga.aux.exception.WrongUserNameException;
import com.ganceanm.allamvizsga.room.model.Room;
import com.ganceanm.allamvizsga.user.model.User;

public interface UserService {
	public User save(User user);
	public User getById(Long userId);
	public User getByUserName(String userName) throws WrongUserNameException;
	public List<User> getByRoom(Room room) throws UserNotFoundException;
	public List<User> getAll();
	public Page<User> findByString(int page, int limit, String params);
	public Page<User> findUnassignedByString(String keyword);
	public ResponseEntity<HttpStatus> createUser(User users) throws NotUniqueUserNameException;
	public ResponseEntity<HttpStatus> putUser(User users) throws NotUniqueUserNameException;
	public ResponseEntity<HttpStatus> deleteUser(Long userId);
	
	public User getByResetToken(String token) throws UserNotFoundException;
	public ResponseEntity<HttpStatus> setRoom(Long userId, String roomNumber);
	
}
