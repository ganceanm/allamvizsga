package com.ganceanm.allamvizsga.user.api.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ganceanm.allamvizsga.room.service.RoomService;
import com.ganceanm.allamvizsga.user.api.message.RegistrationMsg;
import com.ganceanm.allamvizsga.user.model.User;

@Component
public class RegistrationConverter {

	@Autowired
	private RoomService roomService;

	public User toEntity(RegistrationMsg from) {
		User to = new User();

		to.setUserName(from.getEmail());
		to.setFirstName(from.getFirstName());
		to.setLastName(from.getLastName());
		to.setStudentClass(from.getStudentClass());
		to.setStudentYear(from.getStudentYear());
		to.setUserRole(from.getUserRole());
		to.setPhoneNumber(from.getPhoneNumber());
		if (from.getRoomNumber() != null) {
			to.setRoom(roomService.getByRoomNr(from.getRoomNumber()));
		}

		return to;
	}
}
