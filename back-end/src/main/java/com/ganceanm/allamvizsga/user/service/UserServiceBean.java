package com.ganceanm.allamvizsga.user.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.ganceanm.allamvizsga.aux.exception.NotUniqueUserNameException;
import com.ganceanm.allamvizsga.aux.exception.UserNotFoundException;
import com.ganceanm.allamvizsga.aux.exception.WrongUserNameException;
import com.ganceanm.allamvizsga.room.model.Room;
import com.ganceanm.allamvizsga.room.service.RoomService;
import com.ganceanm.allamvizsga.search.SearchCriteria;
import com.ganceanm.allamvizsga.security.service.PasswordEncoder;
import com.ganceanm.allamvizsga.services.email.EmailService;
import com.ganceanm.allamvizsga.user.model.UserSpecification;
import com.ganceanm.allamvizsga.user.model.UserStatus;
import com.ganceanm.allamvizsga.user.model.User;
import com.ganceanm.allamvizsga.user.model.UserPagingRepository;
import com.ganceanm.allamvizsga.user.model.UserRepository;

@Service
public class UserServiceBean implements UserService {

	@Autowired
	private UserRepository usersRepository;

	@Autowired
	private UserPagingRepository usersSortingRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private EmailService emailService;
	
	@Autowired
	private RoomService roomService;

	@Override
	public User save(User user) {
		if(user.getId() == null) {
			user.setCreatedAt(new Date());
		}
		
		user.setModifiedAt(new Date());
		
		return usersRepository.save(user);
	}

	@Override
	public User getById(Long userId) {
		Optional<User> opt = usersRepository.findById(userId);
		
		if(opt.isPresent()) {
			return opt.get();
		} else {
			return null;
		}
	}

	@Override
	public User getByUserName(String userName) throws WrongUserNameException {
		User u = usersRepository.findUserByName(userName);
		if (u == null) {
			throw new WrongUserNameException();
		}
		return u;
	}

	@Override
	public ResponseEntity<HttpStatus> createUser(User users) throws NotUniqueUserNameException {
		User u = usersRepository.findUserByName(users.getUserName());

		if (u == null) {
			u = users;
		} else {
			throw new NotUniqueUserNameException();
		}

		u.setPassword(passwordEncoder.encodePassword(""));
		u.setStatus(UserStatus.Inactive);

		u.setResetToken(UUID.randomUUID().toString());

		String url = "http://localhost:3000/#/setpassword/" + u.getResetToken();

		StringBuilder emailText = new StringBuilder("");
		emailText.append("Hello ").append(u.getFirstName()).append(" ").append(u.getLastName()).append("!\n")
				.append("To set your password click on the following link:\n").append(url);

		emailService.sendEmail(u.getUserName(), "Configure password", emailText.toString());

		try {
			save(u);
			return ResponseEntity.accepted().build();
		} catch (Exception e) {
			throw new NotUniqueUserNameException();
		}
	}
	
	@Override
	public ResponseEntity<HttpStatus> putUser(User user) throws NotUniqueUserNameException {
		try {
			save(user);
			return ResponseEntity.accepted().build();
		} catch (Exception e) {
			throw new NotUniqueUserNameException();
		}
	}

	@Override
	public List<User> getAll() {
		return usersRepository.findAll();
	}

	@Override
	public Page<User> findByString(int page, int limit, String params) {
		List<String> keys = new ArrayList<>();
		keys.add("lastName");
		keys.add("firstName");
		
		List<String> text = Arrays.asList(params.split(" "));

		UserSpecification spec = new UserSpecification(new SearchCriteria(keys, "l:", text, Optional.empty()));

		return usersSortingRepository.findAll(spec, PageRequest.of(page, limit));
	}
	
	@Override
	public Page<User> findUnassignedByString(String keyword) {
		List<String> keys = new ArrayList<>();
		keys.add("lastName");
		keys.add("firstName");
		
		List<String> text = Arrays.asList(keyword.split(" "));

		UserSpecification spec = new UserSpecification(new SearchCriteria(keys, "ua:", text, Optional.empty()));

		return usersSortingRepository.findAll(spec, PageRequest.of(0, 5));
	}

	@Override
	public User getByResetToken(String token) throws UserNotFoundException {
		User u = usersRepository.findUserByResetToken(token);
		if (u == null) {
			throw new UserNotFoundException();
		}
		return u;
	}

	@Override
	public List<User> getByRoom(Room room) throws UserNotFoundException {
		List<User> userList = usersRepository.findUsersByRoomNumber(room);
		
		if(userList == null) {
			throw new UserNotFoundException();
		}
		
		return userList;
	}

	@Override
	public ResponseEntity<HttpStatus> setRoom(Long userId, String roomNumber) {
		User users = getById(userId);
		
		if(roomNumber != null) {
			users.setRoom(roomService.getByRoomNr(roomNumber));
		} else {
			users.setRoom(null);
		}
		save(users);
		
		return ResponseEntity.accepted().build();
	}

	@Override
	public ResponseEntity<HttpStatus> deleteUser(Long userId) {
		try {
			User user = getById(userId);
			
			user.setStatus(UserStatus.Inactive);
			user.setDeleted(true);
			user.setRoom(null);
			
			save(user);
			return ResponseEntity.accepted().build();
		}catch (Exception e) {
			return ResponseEntity.noContent().build();
		}
	}
}
