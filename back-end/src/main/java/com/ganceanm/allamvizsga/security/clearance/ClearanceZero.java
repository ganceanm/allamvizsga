package com.ganceanm.allamvizsga.security.clearance;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAuthority(T(com.ganceanm.allamvizsga.user.model.UserRole).SYS_ADMIN.toString())"
		+ " || hasAuthority(T(com.ganceanm.allamvizsga.user.model.UserRole).ADMIN.toString())" 
		+ " || hasAuthority(T(com.ganceanm.allamvizsga.user.model.UserRole).ECONOMIC.toString())" 
		+ " || hasAuthority(T(com.ganceanm.allamvizsga.user.model.UserRole).ROOM_ADMIN.toString())" 
		+ " || hasAuthority(T(com.ganceanm.allamvizsga.user.model.UserRole).STUDENT.toString())")
public @interface ClearanceZero {
	/*
	 * Security Clearance annotation for authenticated users
	 */
}