package com.ganceanm.allamvizsga.security.model;

public class AuthenticationToken {
	
	private String token;

	public AuthenticationToken() {

	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
