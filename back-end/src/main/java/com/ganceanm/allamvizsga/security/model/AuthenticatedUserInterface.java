package com.ganceanm.allamvizsga.security.model;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import com.ganceanm.allamvizsga.user.model.User;

public interface AuthenticatedUserInterface {
	Authentication getAuthentication();
	UserDetails getPrincipal();
	User getUser();
}
