import { PAYMENTS } from '../types';


export default function payments(state = {}, action) {
    switch (action.type) {
        case PAYMENTS.GET:
            return { ...action.payload };
        default:
            return state;
    }
}