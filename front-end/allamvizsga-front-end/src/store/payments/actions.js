import { PAYMENTS } from "../types";
import store from "../store";
import api from "../../api";

export const getPayments = async (data) => {
    return await api.get(`payment/`, { params: data }).then((response) => {
        store.dispatch({
            type: PAYMENTS.GET,
            payload: response.data
        })

        return response.status;
    })
}

export const postPayment = async (data) => {
    return await api.post(`payment/`, data).then((response) => {
        store.dispatch({
            type: PAYMENTS.POST,
        })

        return response;
    })
}