import { ROOM } from '../../types';

export default function room(state = {}, action) {
    switch (action.type) {
        case ROOM.GET:
            return { ...state, ...action.payload };
        case ROOM.PUT:
            return {}
        default:
            return state;
    }
}