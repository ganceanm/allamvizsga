import { ROOM } from "../../types";
import store from "../../store";
import api from "../../../api";

export const getRoom = async (id) => {
    return await api.get(`room/${id}`).then((response) => {
        store.dispatch({
            type: ROOM.GET,
            payload: response.data
        })

        return response.status;
    })
}

export const putRoom = async (data) => {
    return await api.put(`room/`, data).then((response) => {
        return response.status === 202;
    })
}