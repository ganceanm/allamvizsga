import { ROOMS } from "../types";
import store from "../store";
import api from "../../api";

export const getRooms = async (data) => {
    return await api.get(`room/`, { params: data }).then((response) => {
        store.dispatch({
            type: ROOMS.GET,
            payload: response.data
        })

        return response.status;
    })
}