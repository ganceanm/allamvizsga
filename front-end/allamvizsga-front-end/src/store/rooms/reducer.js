import { ROOMS } from '../types';


export default function rooms(state = {}, action) {
    switch (action.type) {
        case ROOMS.GET:
            return { ...action.payload };
        default:
            return state;
    }
}