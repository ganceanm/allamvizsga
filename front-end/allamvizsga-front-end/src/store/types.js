export const AUTH = {
    LOGGED_IN: "LoggedIn",
    LOGGED_OUT: "LoggedOut",
    LOGIN_EXPIRED: "LoginExpired"
}

export const USER = {
    GET: "GetUser",
    PUT: "PutUser",
    DELETE: "DeleteUser",
}

export const USERS = {
    CLEAR: "ClearUserList",
    GET: "GetUsers",
    POST: "PostUser",
}

export const ME = {
    FETCHED: "Fetched",
    LOGGED_OUT: "LoggedOut",
}

export const ROOMS = {
    GET: "GetRooms",
}

export const ROOM = {
    GET: "GetRoom",
    PUT: "PutRoom"
}

export const PAYMENTS = {
    GET: "GetPayments",
    POST: "PostPayment",
}

export const SOCKET = {
    ON_CONNECT: "OnConnect",
    ON_DISCONNECT: "OnDisonnect",
    ON_MESSAGE: "OnMessage",

}

export const SOCKET_TYPE = {
    USER_JOINED: "UserJoined",
}