import { combineReducers } from 'redux';
import auth from './auth/reducer';
import me from './me/reducer';

import users from './users/reducer';
import user from './users/user/reducer';

import rooms from './rooms/reducer';
import room from './rooms/room/reducer';

import payments from './payments/reducer';

export default combineReducers({
    auth,
    me,
    users: combineReducers({
        list: users,
        element: user
    }),
    rooms: combineReducers({
        list: rooms,
        element: room
    }),
    payments: combineReducers({
        list: payments
    }),
})