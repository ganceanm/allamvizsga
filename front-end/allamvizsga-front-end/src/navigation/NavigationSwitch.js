import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProtectedRoute from '../components/navigation/ProtectedRoute';
import GuestRoute from '../components/navigation/GuestRoute';
import clearence from '../constants/clearence';

import AuthLoading from '../pages/auth/auth';
import Login from '../pages/auth/Login/Login';
import ResetPassword from '../pages/auth/ResetPassword/ResetPassword';
import SetPassword from '../pages/auth/SetPassword/SetPassword';

import Dashboard from '../pages/dashboard/Dashboard';

import Users from '../pages/users/Users';
import UserDetails from '../pages/users/user/User';
import CreateUser from '../pages/users/create/CreateUser';


import Rooms from '../pages/rooms/Rooms';
import RoomDetails from '../pages/rooms/room/Room';

import Payments from '../pages/payments/Payments';
import CreatePayment from '../pages/payments/create/CreatePayment';

const NavigationSwitch = (props) => (
    <Switch>
        <ProtectedRoute path="/dashboard"
            toggle={props.drawerToggle}
            component={Dashboard}
            requiredRole={clearence.Zero} />

        <ProtectedRoute path="/users/create"
            toggle={props.drawerToggle}
            component={CreateUser}
            requiredRole={clearence.Three} />
        <ProtectedRoute path="/users/:id"
            toggle={props.drawerToggle}
            component={UserDetails}
            requiredRole={clearence.Two} />
        <ProtectedRoute path="/users"
            toggle={props.drawerToggle}
            component={Users}
            requiredRole={clearence.Two} />

        <ProtectedRoute path="/rooms/:id"
            toggle={props.drawerToggle}
            component={RoomDetails}
            requiredRole={clearence.Two} />
        <ProtectedRoute path="/rooms"
            toggle={props.drawerToggle}
            component={Rooms}
            requiredRole={clearence.Two} />

        <ProtectedRoute path="/payments/create"
            toggle={props.drawerToggle}
            component={CreatePayment}
            requiredRole={clearence.Four} />
        <ProtectedRoute path="/payments"
            toggle={props.drawerToggle}
            component={Payments}
            requiredRole={clearence.Four} />

        {/* <ProtectedRoute path="/reports/create"
            toggle={props.drawerToggle}
            component={Dashboard}
            requiredRole={clearence.Zero} />
        <ProtectedRoute path="/reports/:id"
            toggle={props.drawerToggle}
            component={Dashboard}
            requiredRole={clearence.Zero} />
        <ProtectedRoute path="/reports"
            toggle={props.drawerToggle}
            component={Dashboard}
            requiredRole={clearence.Zero} /> */}


        <GuestRoute path="/setpassword/:id" component={SetPassword} />
        <GuestRoute path="/resetpassword" component={ResetPassword} />
        <GuestRoute path="/login" component={Login} />

        <Route path="/" component={AuthLoading} />
    </Switch>
)

export default NavigationSwitch;