import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { IconButton, ListItemText, Typography, CircularProgress, Card, CardContent, CardActions, Table, TableHead, TableRow, TableCell, TableBody, Tooltip } from '@material-ui/core';

import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

import { useHistory, useLocation } from 'react-router-dom';
import { APP_BAR_HEIGHT } from '../../../constants/theme';
import moment from 'moment';

import { PAYMENT_TYPE } from "../../../types"
import Confirm from '../../../components/Confirm';

const useStyles = makeStyles(theme => ({
    content: {
        marginTop: APP_BAR_HEIGHT,
        marginBottom: 60,
        width: "100%",
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },

    contentCentered: {
        display: "flex",
        marginTop: APP_BAR_HEIGHT,
        width: "100%",
        height: "50%",
        alignItems: "center",
        justifyContent: "center",
    },

    card: {
        marginLeft: "auto",
        marginRight: "auto",
        maxWidth: theme.breakpoints.values.lg,
    },

    tableHead: {
        fontWeight: "bold",
        fontSize: 16
    },

    list: {
        width: "100%",
        padding: 0
    },

    cardActions: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        justifyContent: "flex-end"
    },

    noResultText: {
        fontSize: 20,
        display: "flex",
        textAlignHorizontal: "center",
        textAlign: "center",
        opacity: 0.4
    },

    viewButton: {
        color: theme.palette.primary.light
    },

}))

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

function Content(props) {

    const classes = useStyles();
    const history = useHistory();

    const { page, pageCount, total, values } = useSelector(state => state.payments.list)

    const { isLoading } = props;

    const query = useQuery();
    const limit = query.get("limit") ? query.get("limit") : 15;

    const changePage = (num) => {
        history.push({
            pathname: `/payments`,
            search: `?page=${num}&limit=${query.get("limit") ? query.get("limit") : 15}&keyword=${query.get("keyword") ? query.get("keyword") : ""}`
        })
    }

    const parseForLabel = (user) => {
        if (user.studentClass && user.studentYear) {
            return user.studentClass + " " + user.studentYear;
        } else {
            return "Nem hallgató"
        }
    }

    return (
        <div className={values && values.length > 0 ? classes.content : classes.contentCentered}>
            {values && values.length > 0 ?
                <Card className={classes.card}>
                    <CardContent>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell className={classes.tableHead}>Szorszám</TableCell>
                                    <TableCell className={classes.tableHead}>Felhasználó</TableCell>
                                    <TableCell className={classes.tableHead}>Összeg</TableCell>
                                    <TableCell className={classes.tableHead}>Kasszaszalag száma</TableCell>
                                    <TableCell className={classes.tableHead}>Fizetett hónap</TableCell>
                                    <TableCell className={classes.tableHead}>Kifizetés időpontja</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {values.map((item) => {
                                    return (
                                        <TableRow size="small" key={item.id}>
                                            <TableCell>
                                                <ListItemText primary={item.id} />
                                            </TableCell>
                                            <TableCell>
                                                <Tooltip title={parseForLabel(item.user)} placement="bottom-start">
                                                    <ListItemText primary={item.user.lastName + " " + item.user.firstName} />
                                                </Tooltip>
                                            </TableCell>
                                            <TableCell>
                                                <ListItemText primary={item.value + " RON"} />
                                            </TableCell>
                                            <TableCell>
                                                <ListItemText
                                                    secondary={item.receiptNr + "/" + moment(item.timestamp).format("DD.MM.YYYY")} />
                                            </TableCell>
                                            <TableCell>
                                                {item.type === PAYMENT_TYPE.ROOM.value ?
                                                    <Tooltip title={item.paidRoom + ". szoba"} placement="bottom-start">
                                                        <ListItemText
                                                            secondary={moment().month(item.month - 1).format("MMMM") + " hónap"} />
                                                    </Tooltip>
                                                    :
                                                    <ListItemText
                                                        secondary={"Nem bentlakás"} />
                                                }
                                            </TableCell>
                                            <TableCell>
                                                <ListItemText
                                                    secondary={moment(item.timestamp).format("YYYY. MMM. DD. HH:mm")} />
                                            </TableCell>
                                            <TableCell align="right">
                                                <Confirm
                                                    icon
                                                    // confirm={submit}
                                                    content={<DeleteOutlinedIcon fontSize="small" />}
                                                />
                                            </TableCell>
                                        </TableRow>
                                    )
                                })
                                }
                            </TableBody>
                        </Table>
                    </CardContent>
                    <CardActions>
                        <div className={classes.cardActions}>
                            <Typography color="textSecondary">
                                {page * limit + 1}-{page * limit + values.length} / {total} kifizetés
                                </Typography>

                            <IconButton onClick={() => changePage(page - 1)}
                                size="small"
                                disabled={page === 0}>
                                <NavigateBeforeIcon />
                            </IconButton>
                            <IconButton onClick={() => changePage(page + 1)}
                                size="small"
                                disabled={page + 1 === pageCount}>
                                <NavigateNextIcon />
                            </IconButton>
                        </div>
                    </CardActions>

                </Card>
                :
                isLoading ?
                    <CircularProgress color={"primary"} />
                    :
                    <Typography className={classes.noResultText}>Nem található a keresési feltételeknek megfelelő kifizetés.</Typography>
            }
        </div >
    )
}

export default Content;
