import * as yup from 'yup';

export const validation = yup.object().shape({
    type: yup.string().required("A mező kitöltése kötelező!"),
    month: yup.number(),
    value: yup.number().typeError("A megadott érték nem szám!").required("A mező kitöltése kötelező!").positive(),
    receiptNr: yup.number().typeError("A megadott érték nem szám!").required("A mező kitöltése kötelező!").positive(),
})