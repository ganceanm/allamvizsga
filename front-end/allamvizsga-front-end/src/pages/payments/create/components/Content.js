import React, { useState } from "react";
import { makeStyles, Card, CardContent, TextField, CardActions, Button, CardHeader, FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Formik, Form } from "formik";
import { validation } from "./validation";
import WithAlert from "../../../../components/functional/withAlert";
import { APP_BAR_HEIGHT } from "../../../../constants/theme";
import { MONTHS, PAYMENT_TYPE } from "../../../../types";
import moment from "moment";
import PaymentUserSearch from "../../../../components/PaymentUserSearch";
import { postPayment } from "../../../../store/payments/actions";

const useStyles = makeStyles(theme => ({
	content: {
		marginTop: APP_BAR_HEIGHT,
		width: "100%",
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingBottom: theme.spacing(4),
	},

	card: {
		margin: theme.spacing(2),
		padding: theme.spacing(2),
		borderRadius: theme.shape.borderRadius,
		backgroundColor: theme.palette.background.paper,
		// height: "100%",
		display: "flex",
		flexDirection: "column",
		width: "100%",
		maxWidth: theme.breakpoints.values.md,
	},

	textInput: {
		margin: theme.spacing(1),
		minWidth: 140,
		width: "100%"
	},

	buttonBox: {
		marginTop: theme.spacing(2),
		justifyContent: "flex-end",
		marginRight: theme.spacing(2)
	},

	selectorWrapper: {
		display: "flex",
		justifyContent: "space-between",
		width: "100%",
	},
}));

const Content = (props) => {
	const { showAlert } = props;

	const classes = useStyles();
	let history = useHistory();

	const [user, setUser] = useState(null)

	const onSubmit = async (values) => {
		const data = {
			...values,
			user: user.userId
		}


		const result = await postPayment(data)
		console.log("res", result)
		if (result.status === 202) {
			showAlert("general.saveSuccess")
			setTimeout(() => history.goBack(), 3000);
		} else if (result.status === 409) {
			showAlert("payment.monthPaid");
		} else if (result.status === 404) {
			showAlert("payment.userError");
		} else {
			showAlert("general.default");
		}
	}

	const mapMenuItems = (obj) => {
		const toReturn = [];

		for (let [key, value] of Object.entries(obj)) {
			if (key) {
				toReturn.push(value)
			}
		}

		return toReturn;
	}

	return (
		<div className={classes.content}>
			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				initialValues={{
					type: "General",
					month: moment().get("month") + 1,
					value: "",
					receiptNr: ""
				}}
				onSubmit={(values, { setSubmitting }) => onSubmit(values)}
				validationSchema={validation}
			>
				{formikProps => {
					const { values, handleChange, handleBlur, handleSubmit, errors } = formikProps;
					return (
						<Form onSubmit={handleSubmit}>
							<Card className={classes.card}>
								<CardHeader title={"Új kifizetés"} />
								<CardContent>
									<PaymentUserSearch setUser={setUser} />

									<div className={classes.selectorWrapper}>
										<FormControl className={classes.textInput}>
											<InputLabel id="label">
												Típus
                                            </InputLabel>
											<Select
												labelId="label"
												id="type"
												value={values.type}
												onChange={(event) => handleChange({ target: { value: event.target.value, name: "type" } })}
												onBlur={handleBlur}
												error={!!errors.type}
											>
												{mapMenuItems(PAYMENT_TYPE).map((item) => {
													return <MenuItem key={item.value} value={item.value}>{item.display}</MenuItem>
												})}
											</Select>
										</FormControl>

										<FormControl className={classes.textInput}>
											<InputLabel id="demo-simple-select-outlined-label">
												Hónap
                                            </InputLabel>
											<Select
												labelId="demo-simple-select-outlined-label"
												id="month"
												value={values.month}
												onChange={(event) => handleChange({ target: { value: event.target.value, name: "month" } })}
												onBlur={handleBlur}
												error={!!errors.month}
												helperText={errors.month}
												disabled={values.type === "General"}
											>
												{mapMenuItems(MONTHS).map((item) => {
													return <MenuItem key={item.value} value={item.value}>{item.display}</MenuItem>
												})}
											</Select>
										</FormControl>
									</div>

									<div className={classes.selectorWrapper}>
										<TextField id="value"
											label="Összeg"
											className={classes.textInput}
											required

											value={values.value}
											onChange={handleChange}
											onBlur={handleBlur}
											error={!!errors.value}
											helperText={errors.value}
										/>
										<TextField id="receiptNr"
											label="Kassaszalag száma"
											className={classes.textInput}
											required

											value={values.receiptNr}
											onChange={handleChange}
											onBlur={handleBlur}
											error={!!errors.receiptNr}
											helperText={errors.receiptNr}
										/>
									</div>

								</CardContent>
								<CardActions className={classes.buttonBox}>
									<Button type="submit" variant="contained" color="primary" disabled={!user}>
										Mentés
                                </Button>
								</CardActions>
							</Card>
						</Form>
					)
				}}
			</Formik>
		</div>


	)
}

export default WithAlert(Content)