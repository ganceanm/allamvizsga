import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Header from './components/Header';
import Content from './components/Content';
import { clearUserList } from '../../../store/users/actions';


const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        height: "100%",
        overflowX: "hidden",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "center"
    },
}))

function CreatePayment(props) {

    const { toggle } = props;

    const classes = useStyles();

    useEffect(() => {
        return function cleanup() {
            clearUserList()
        };
    })

    return (
        <div className={classes.root}>
            <Header toggle={toggle} />
            <Content />
        </div >
    )
}

export default CreatePayment;