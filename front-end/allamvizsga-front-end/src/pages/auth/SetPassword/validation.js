import * as yup from 'yup';

export const validation = yup.object().shape({
    newPassword: yup.string().min(6, "A jelszó hossza min. 6 karakter!").required("A mező kitöltése kötelező!"),
    passwordAgain: yup.string().oneOf([yup.ref('newPassword'), null], 'A két jelszó nem egyezik')
})