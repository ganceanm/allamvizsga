import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Header from './components/Header';
import { useParams } from 'react-router-dom';
import { getRoom } from '../../../store/rooms/room/actions';
import store from '../../../store/store';
import { ROOM } from '../../../store/types';
import Content from './components/Content';
import { useSelector } from 'react-redux';
import { clearUserList } from '../../../store/users/actions';

const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        height: "100%",
        overflowX: "hidden",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "center"
    },
}));

function RoomDetails(props) {
    const { toggle } = props;

    const classes = useStyles();

    const roomId = useParams().id;

    useEffect(() => {
        store.dispatch({
            type: ROOM.PUT,
        })
        getRoom(roomId)
    }, [roomId])

    useEffect(() => {
        return function cleanup() {
            clearUserList()
        };
    })

    const room = useSelector(state => state.rooms.element)

    return (
        <div className={classes.root}>
            <Header toggle={toggle} />
            {room.roomNumber && <Content />}
        </div >
    )
}

export default RoomDetails;
