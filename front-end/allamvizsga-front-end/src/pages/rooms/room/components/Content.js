import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { IconButton, Typography, Card, CardHeader, FormControl, InputLabel, Select, MenuItem, Paper, List, ListItem, ListItemText, Avatar, CircularProgress, CardContent, CardActions } from '@material-ui/core';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';


import { useHistory } from 'react-router-dom';
import { APP_BAR_HEIGHT } from "../../../../constants/theme";
import { useSelector } from 'react-redux';
import SaveSearch from '../../../../components/SaveSearch';
import { getUsers } from '../../../../store/users/actions';
import Confirm from '../../../../components/Confirm';
import { putRoom } from '../../../../store/rooms/room/actions';
import WithAlert from '../../../../components/functional/withAlert';
import store from '../../../../store/store';
import { ROOM } from '../../../../store/types';

const useStyles = makeStyles(theme => ({


    content: {
        marginTop: APP_BAR_HEIGHT,
        width: "100%",
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },

    card: {
        margin: "auto",
        padding: theme.spacing(2),
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.background.paper,
        display: "flex",
        flexDirection: "column",
        // minWidth: theme.breakpoints.values.sm,
        // width: "100%",
        maxWidth: theme.breakpoints.values.sm,
    },

    formControl: {
        minWidth: 140
    },

    listTitleContainer: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1),
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%"
    },

    searchContainer: {
        marginBottom: theme.spacing(1)
    },

    tooltip: {
        position: "fixed",
        zIndex: 500
    },

    list: {
        width: "100%",
        padding: 0,
        // marginTop: theme.spacing(3)
    },

    listItem: {
        // backgroundColor: theme.palette.background.paper,
        // width: 400,
        display: "flex",
        justifyContent: "space-between",
        width: theme.breakpoints.values.sm - 200,
    },
    listItemContent: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    avatar: {
        backgroundColor: theme.palette.secondary.light,
        marginRight: theme.spacing(1)
    },
    classText: {
        marginLeft: theme.spacing(2)
    },

    roomText: {
        marginLeft: theme.spacing(2)
    },

    listIconButton: {
        color: theme.palette.black
    },

    noResultText: {
        width: theme.breakpoints.values.sm - 200,
        fontSize: 20,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: theme.spacing(2),
        opacity: 0.4
    },

    cardActions: {
        display: "flex",
        justifyContent: "flex-end",
    }
}))

function Content(props) {

    const { showAlert } = props;

    const classes = useStyles();
    const history = useHistory();
    const room = useSelector(state => state.rooms.element);
    const userResult = useSelector(state => state.users.list);

    const [beds, setBeds] = useState(room.roomStatus === "Active" ? room.roomType === "Three" ? 3 : 4 : 0);
    const [showSearch, setShowSearch] = useState(false);
    const [userList, setUserList] = useState(room.userList);

    const handleUserPicked = (user) => {
        userList.push(user)
        setShowSearch(false)
    }

    const handleSearch = async (keyword) => {
        await getUsers({
            page: 0,
            limit: 5,
            keyword: keyword ? keyword : "",
            unassigned: true
        });
    }

    const handleKickUser = (user) => {
        setUserList(userList.filter(item => item.userId !== user.userId))
    }

    const submit = async () => {
        let processedList = userList.map(item => item.userId);

        const data = {
            id: room.id,
            users: processedList,
            beds: beds
        }

        if (data.users.length > beds) {
            showAlert("room.tooManyUsers");
            return;
        }

        const result = await putRoom(data);

        if (result) {
            showAlert("general.saveSuccess")
            setTimeout(() => {
                store.dispatch({
                    type: ROOM.PUT,
                })
                history.goBack()
            }, 3000)
        } else {
            showAlert("general.default")
        }
    }

    return (
        <div className={classes.content}>
            {room.roomNumber ?
                <Card className={classes.card}>
                    <CardHeader title={room.roomNumber + ". szoba"} />
                    <CardContent>
                        <FormControl required className={classes.formControl}>
                            <InputLabel id="select-required-label">Ágyak</InputLabel>
                            <Select
                                labelId="select-required-label"
                                id="beds"
                                value={beds}
                                onChange={(event) => setBeds(event.target.value)}
                                className={classes.selectEmpty}
                            >
                                <MenuItem value={0}>
                                    <em>Egy sem</em>
                                </MenuItem>
                                <MenuItem value={3}>Három</MenuItem>
                                <MenuItem value={4}>Négy</MenuItem>
                            </Select>
                        </FormControl>

                        <div className={classes.listTitleContainer}>
                            <Typography style={{ fontWeight: "bold" }}>
                                Bentlakók
                            </Typography>
                            <IconButton onClick={() => setShowSearch(true)}
                                className={classes.listIconButton}
                                disabled={userList && beds <= userList.length}>
                                <PersonAddIcon />
                            </IconButton>
                        </div>

                        {showSearch &&
                            <div className={classes.searchContainer}>
                                <SaveSearch onSubmit={handleSearch} onClose={() => setShowSearch(false)} />
                                {userResult &&
                                    <Paper className={classes.tooltip}>
                                        <List>
                                            {userResult.values && userResult.values.length > 0 ?
                                                userResult.values.filter(item => !userList.map(item => item.userId).includes(item.userId)).map((item) => {
                                                    return (
                                                        <ListItem
                                                            key={item.userId}
                                                            className={classes.listItem}
                                                            button
                                                            onClick={() => handleUserPicked(item)}>
                                                            <div className={classes.listItemContent}>
                                                                <ListItemText primary={item.lastName + " " + item.firstName} />
                                                                <ListItemText className={classes.classText}
                                                                    secondary={item.studentClass ? item.studentClass + " " + item.studentYear : "Nem hallgató"} />
                                                                <ListItemText className={classes.classText}
                                                                    secondary={item.roomNumber ? item.roomNumber : "Nem bentlakó"} />
                                                            </div>
                                                        </ListItem>)
                                                })
                                                :
                                                userResult.page === 0 ?
                                                    <ListItem
                                                        key={0}
                                                        className={classes.listItem}>
                                                        <div className={classes.listItemContent}>
                                                            <ListItemText primary={"Nincs találat"} />
                                                        </div>
                                                    </ListItem>
                                                    : null
                                            }
                                        </List>
                                    </Paper>
                                }
                            </div>
                        }

                        {userList && userList.length > 0 ?
                            <List className={classes.list}>
                                {userList.map((item) => {
                                    return (
                                        <ListItem key={item.userId} className={classes.listItem}>
                                            <div className={classes.listItemContent}>
                                                <Avatar className={classes.avatar}>{item.lastName[0] + item.firstName[0]}</Avatar>
                                                <ListItemText primary={item.lastName + " " + item.firstName} />
                                                <ListItemText className={classes.classText}
                                                    secondary={item.studentClass ? item.studentClass + " " + item.studentYear : "Nem hallgató"} />
                                            </div>
                                            <IconButton onClick={() => handleKickUser(item)}
                                                className={classes.listIconButton}>
                                                <DeleteForeverIcon />
                                            </IconButton>
                                        </ListItem>
                                    )
                                })
                                }
                            </List>
                            :
                            <Typography className={classes.noResultText}>A szoba üres</Typography>
                        }
                    </CardContent>
                    <CardActions className={classes.cardActions}>
                        <Confirm
                            confirm={submit}
                            content={"Mentés"}
                        />
                    </CardActions>
                </Card>

                :
                <CircularProgress color={"primary"} />
            }
        </div >
    )
}

export default WithAlert(Content);
