import React from "react";
import { makeStyles, Card, CardContent, TextField, CardActions, Button, CardHeader } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Formik, Form } from "formik";
import { validation } from "./validation";
import WithAlert from "../../../../components/functional/withAlert";
import { APP_BAR_HEIGHT } from "../../../../constants/theme";
import DropdownAutocomplete from "../../../../components/input/DropdownAutocomplete";
import { STUDENT_CLASS } from "../../../../constants/enum/StudentClass";
import { STUDENT_YEAR } from "../../../../constants/enum/StudentYear";
import { USER_ROLE } from "../../../../constants/enum/UserRole";
import roomNumbers from "../../../../constants/roomNumbers";
import { postUser } from "../../../../store/users/actions";

const useStyles = makeStyles(theme => ({
    content: {
        marginTop: APP_BAR_HEIGHT,
        width: "100%",
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },

    card: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.background.paper,
        // height: "100%",
        display: "flex",
        flexDirection: "column",
        width: "100%",
        maxWidth: theme.breakpoints.values.md,
    },

    textInput: {
        margin: theme.spacing(1),
        minWidth: 140,
        width: "100%"
    },

    buttonBox: {
        marginTop: theme.spacing(2),
        justifyContent: "flex-end",
        marginRight: theme.spacing(2)
    },

    selectorWrapper: {
        display: "flex",
        justifyContent: "space-between",
        width: "100%",
    },
}));

const Content = (props) => {
    const { showAlert } = props;

    const classes = useStyles();
    let history = useHistory();

    const onSubmit = async (values) => {
        const data = {
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email,
            phoneNumber: values.phone,
            userRole: values.userRole,
            roomNumber: values.roomNumber === "0" ? null : Number(values.roomNumber),
            studentClass: values.studentClass === "0" ? null : values.studentClass,
            studentYear: values.studentYear === "0" ? null : values.studentYear,
        }

        const result = await postUser(data)

        if (result) {
            showAlert("general.saveSuccess")
            setTimeout(() => history.goBack(), 3000)
        } else {
            showAlert("users.notuniqueemail")
        }

    }

    const listMenuItems = (list, selected) => {
        const toReturn = [];

        if (list === STUDENT_CLASS) {
            toReturn.push({ display: "Nem hallgató", value: "0" })
            list.map(item => toReturn.push(item));
        } else if (list === roomNumbers) {
            toReturn.push({ display: "Nem bentlakó", value: "0" })
            list.map(item => toReturn.push({ value: item + "" }))
        } else if (list === STUDENT_YEAR) {

            const selectedObj = STUDENT_CLASS.filter((item) => item.value === selected)[0]
            toReturn.push({ display: "Nem hallgató", value: "0" })
            if (selected === "0") return toReturn;
            list.map(item => item.hidden <= selectedObj.years && toReturn.push(item));
        } else {
            list.map(item => toReturn.push(item));
        }

        return toReturn;
    }

    return (
        <div className={classes.content}>
            <Formik
                validateOnChange={false}
                validateOnBlur={false}
                initialValues={{
                    firstName: "",
                    lastName: "",
                    email: '',
                    phone: "",
                    roomNumber: "0",
                    userRole: "0",
                    studentClass: "0",
                    studentYear: "0"
                }}
                onSubmit={(values, { setSubmitting }) => onSubmit(values)}
                validationSchema={validation}
            >
                {formikProps => {
                    const { values, handleChange, handleBlur, handleSubmit, errors } = formikProps;
                    return (
                        <Form onSubmit={handleSubmit}>
                            <Card className={classes.card}>
                                <CardHeader title={"Új felhasználó"} />
                                <CardContent>
                                    <div className={classes.selectorWrapper}>
                                        <TextField id="lastName"
                                            label="Vezetéknév"
                                            variant="outlined"
                                            className={classes.textInput}
                                            required

                                            value={values.lastName}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!errors.lastName}
                                            helperText={errors.lastName}
                                        />
                                        <TextField id="firstName"
                                            label="Keresztnév"
                                            variant="outlined"
                                            className={classes.textInput}
                                            required

                                            value={values.firstName}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!errors.firstName}
                                            helperText={errors.firstName}
                                        />
                                    </div>

                                    <div className={classes.selectorWrapper}>
                                        <TextField id="email"
                                            label="E-mail cím"
                                            variant="outlined"
                                            className={classes.textInput}
                                            required

                                            value={values.email}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!errors.email}
                                            helperText={errors.email}
                                        />
                                        <TextField id="phone"
                                            label="Telefonszám"
                                            variant="outlined"
                                            className={classes.textInput}
                                            required

                                            value={values.phone}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!errors.phone}
                                            helperText={errors.phone}
                                        />
                                    </div>

                                    <div className={classes.selectorWrapper}>
                                        <DropdownAutocomplete
                                            id="userRole"
                                            label="Szerepkör"
                                            values={listMenuItems(USER_ROLE)}

                                            value={values.userRole}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!errors.userRole}
                                            helpertext={errors.userRole}
                                        />

                                        <DropdownAutocomplete
                                            id="roomNumber"
                                            label="Szobaszám"
                                            values={listMenuItems(roomNumbers)}

                                            value={values.roomNumber}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!errors.roomNumber}
                                            helpertext={errors.roomNumber}
                                        />
                                    </div>

                                    <div className={classes.selectorWrapper}>
                                        <DropdownAutocomplete
                                            id="studentClass"
                                            label="Szak"
                                            values={listMenuItems(STUDENT_CLASS)}

                                            value={values.studentClass}
                                            onChange={(data) => {
                                                handleChange(data);
                                                if (data.target.value === "0") {
                                                    handleChange({
                                                        target: {
                                                            value: "0",
                                                            id: "studentYear"
                                                        }
                                                    })
                                                }
                                            }}
                                            onBlur={handleBlur}
                                            error={!!errors.studentClass}
                                            helpertext={errors.studentClass}
                                        />

                                        <DropdownAutocomplete
                                            id="studentYear"
                                            label="Évfolyam"
                                            values={listMenuItems(STUDENT_YEAR, values.studentClass)}
                                            disabled={values.studentClass === "0"}
                                            value={values.studentYear}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={errors.studentYear}
                                            helpertext={errors.studentYear}
                                        />


                                    </div>

                                </CardContent>
                                <CardActions className={classes.buttonBox}>
                                    <Button type="submit" variant="contained" color="primary">
                                        Mentés
                                </Button>
                                </CardActions>
                            </Card>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    )
}

export default WithAlert(Content)