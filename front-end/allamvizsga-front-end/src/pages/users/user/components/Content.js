import React from "react";
import { makeStyles, Card, CardContent, TextField, CardActions, Button, CardHeader, CircularProgress } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Formik, Form } from "formik";
import { validation } from "./validation";
import WithAlert from "../../../../components/functional/withAlert";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { APP_BAR_HEIGHT } from "../../../../constants/theme";
import DropdownAutocomplete from "../../../../components/input/DropdownAutocomplete";
import { STUDENT_CLASS } from "../../../../constants/enum/StudentClass";
import { STUDENT_YEAR } from "../../../../constants/enum/StudentYear";
import { USER_ROLE } from "../../../../constants/enum/UserRole";
import { useSelector } from "react-redux";
import roomNumbers from "../../../../constants/roomNumbers";
import { putUser, deleteUser } from "../../../../store/users/user/actions";
import Confirm from "../../../../components/Confirm";

const useStyles = makeStyles(theme => ({
    content: {
        marginTop: APP_BAR_HEIGHT,
        width: "100%",
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },

    card: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.background.paper,
        // height: "100%",
        display: "flex",
        flexDirection: "column",
        width: "100%",
        maxWidth: theme.breakpoints.values.md,
    },

    textInput: {
        margin: theme.spacing(1),
        minWidth: 140,
        width: "100%"
    },

    buttonBox: {
        marginTop: theme.spacing(2),
        justifyContent: "space-between",
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2)
    },

    deleteButton: {
        color: "red"
    },

    selectorWrapper: {
        display: "flex",
        justifyContent: "space-between",
        width: "100%",
    },
}));

const Content = (props) => {
    const { showAlert } = props;

    const classes = useStyles();
    let history = useHistory();

    const user = useSelector(state => state.users.element)

    const onSubmit = async (values) => {
        const data = {
            id: user.id,
            firstName: values.firstName,
            lastName: values.lastName,
            userName: values.email,
            phoneNumber: values.phone,
            studentClass: values.studentClass === "0" ? null : values.studentClass.value ? values.studentClass.value : values.studentClass,
            studentYear: values.studentYear === "0" ? null : values.studentYear.value ? values.studentYear.value : values.studentYear,
        }


        const result = await putUser(data)

        if (result) {
            showAlert("general.saveSuccess")
            setTimeout(() => history.goBack(), 3000)
        } else {
            showAlert("users.notuniqueemail")
        }

    }

    const _deleteUser = async () => {
        const result = await deleteUser(user.id)

        if (result) {
            showAlert("general.saveSuccess")
            setTimeout(() => history.goBack(), 3000)
        } else {
            showAlert("general.default")
        }
    }

    const listMenuItems = (list, selected) => {
        const toReturn = [];

        if (list === STUDENT_CLASS) {
            toReturn.push({ display: "Nem hallgató", value: "0" })
            list.map(item => toReturn.push(item));
        } else if (list === roomNumbers) {
            toReturn.push({ display: "Nem bentlakó", value: "0" })
            list.map(item => toReturn.push({ value: item + "" }))
        } else if (list === STUDENT_YEAR) {
            const selectedObj = STUDENT_CLASS.filter((item) => item.value === selected)[0]
            toReturn.push({ display: "Nem hallgató", value: "0" })
            if (selected === "0") return toReturn;
            if (!selectedObj) {
                list.map(item => toReturn.push(item))
            } else {
                list.map(item => item.hidden <= selectedObj.years && toReturn.push(item));
            }
        } else {
            list.map(item => toReturn.push(item));
        }

        return toReturn;
    }

    return (
        <div className={classes.content}>
            {user.firstName ?
                <Formik
                    initialValues={{
                        firstName: user.firstName,
                        lastName: user.lastName,
                        email: user.userName,
                        phone: user.phoneNumber,
                        roomNumber: user.room ? { value: user.room.roomNumber } : { display: "Nem bentlakó", value: "0" },
                        userRole: user.userRole ? listMenuItems(USER_ROLE).filter(item => item.value === user.userRole)[0] : "0",
                        studentClass: user.studentClass ? listMenuItems(STUDENT_CLASS).filter(item => item.value === user.studentClass)[0] : { display: "Nem hallgató", value: "0" },
                        studentYear: user.studentYear ? listMenuItems(STUDENT_YEAR).filter(item => item.value === user.studentYear)[0] : { display: "Nem hallgató", value: "0" }
                    }}
                    onSubmit={(values, { setSubmitting }) => onSubmit(values)}
                    validationSchema={validation}
                >
                    {formikProps => {
                        const { values, handleChange, handleBlur, handleSubmit, errors } = formikProps;
                        console.log(values)
                        return (
                            <Form onSubmit={handleSubmit}>
                                <Card className={classes.card}>
                                    <CardHeader title={"Felhasználó szerkesztése"} />
                                    <CardContent>
                                        <div className={classes.selectorWrapper}>
                                            <TextField id="lastName"
                                                label="Vezetéknév"
                                                variant="outlined"
                                                className={classes.textInput}
                                                required

                                                value={values.lastName}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={!!errors.lastName}
                                                helperText={errors.lastName}
                                            />
                                            <TextField id="firstName"
                                                label="Keresztnév"
                                                variant="outlined"
                                                className={classes.textInput}
                                                required

                                                value={values.firstName}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={!!errors.firstName}
                                                helperText={errors.firstName}
                                            />
                                        </div>

                                        <div className={classes.selectorWrapper}>
                                            <TextField id="email"
                                                label="E-mail cím"
                                                variant="outlined"
                                                className={classes.textInput}
                                                required

                                                value={values.email}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={!!errors.email}
                                                helperText={errors.email}
                                            />
                                            <TextField id="phone"
                                                label="Telefonszám"
                                                variant="outlined"
                                                className={classes.textInput}
                                                required

                                                value={values.phone}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={!!errors.phone}
                                                helperText={errors.phone}
                                            />
                                        </div>

                                        <div className={classes.selectorWrapper}>
                                            <DropdownAutocomplete
                                                id="userRole"
                                                label="Szerepkör"
                                                values={listMenuItems(USER_ROLE)}
                                                disabled
                                                defval={values.userRole}
                                                value={values.userRole}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={!!errors.userRole}
                                                helpertext={errors.userRole}
                                            />

                                            <DropdownAutocomplete
                                                id="roomNumber"
                                                label="Szobaszám"
                                                values={listMenuItems(roomNumbers)}
                                                disabled
                                                defval={values.roomNumber}
                                                value={values.roomNumber}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={!!errors.roomNumber}
                                                helpertext={errors.roomNumber}
                                            />
                                        </div>

                                        <div className={classes.selectorWrapper}>
                                            <DropdownAutocomplete
                                                id="studentClass"
                                                label="Szak"
                                                values={listMenuItems(STUDENT_CLASS)}
                                                defval={values.studentClass}
                                                value={values.studentClass}
                                                onChange={(data) => {
                                                    handleChange(data);
                                                    if (data.target.value === "0") {
                                                        handleChange({
                                                            target: {
                                                                value: "0",
                                                                id: "studentYear"
                                                            }
                                                        })
                                                    }
                                                }}
                                                onBlur={handleBlur}
                                                error={!!errors.studentClass}
                                                helpertext={errors.studentClass}
                                            />

                                            <DropdownAutocomplete
                                                id="studentYear"
                                                label="Évfolyam"
                                                values={listMenuItems(STUDENT_YEAR, values.studentClass)}
                                                disabled={values.studentClass === "0"}
                                                defval={values.studentYear}
                                                value={values.studentYear}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                error={errors.studentYear}
                                                helpertext={errors.studentYear}
                                            />


                                        </div>

                                    </CardContent>
                                    <CardActions className={classes.buttonBox}>
                                        <Confirm confirm={_deleteUser}
                                            icon
                                            content={(<DeleteOutlineIcon className={classes.deleteButton} />)}
                                        />
                                        <Button type="submit" variant="contained" color="primary">
                                            Mentés
                                        </Button>
                                    </CardActions>
                                </Card>
                            </Form>
                        )
                    }}
                </Formik>
                :
                <CircularProgress color={"primary"} />}
        </div>
    )
}

export default WithAlert(Content)