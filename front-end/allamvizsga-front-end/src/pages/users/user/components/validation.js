import * as yup from 'yup';

export const validation = yup.object().shape({
    firstName: yup.string().required("A mező kitöltése kötelező!"),
    lastName: yup.string().required("A mező kitöltése kötelező!"),
    email: yup.string().required("A mező kitöltése kötelező!").email("Érvénytelen e-mail cím!"),
    phone: yup.string().required("A mező kitöltése kötelező!"),
    studentClass: yup.string().required("A mező kitöltése kötelező!"),
    studentYear: yup.string().required("A mező kitöltése kötelező!")
        .test("class-year-test", "Válasszon évfolyamot!", function test(studentYear) {
            if (studentYear !== "0") {
                return true;
            } else if (this.parent.studentClass !== "0") {
                return false;
            } else return true;
        }),
})