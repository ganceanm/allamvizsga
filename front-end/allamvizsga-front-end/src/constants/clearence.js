const clearence = {
    Zero: [
        "STUDENT",
        "SYS_ADMIN"
    ],

    One: [
        "SYS_ADMIN"
    ],

    Two: [
        "ROOM_ADMIN",
        "ADMIN",
        "ECONOMIC",
        "SYS_ADMIN"
    ],

    Three: [
        "ADMIN",
        "ECONOMIC",
        "SYS_ADMIN"
    ],

    Four: [
        "ECONOMIC",
        "SYS_ADMIN"
    ],

    Five: [
        "SYS_ADMIN"
    ],
}

export default clearence;