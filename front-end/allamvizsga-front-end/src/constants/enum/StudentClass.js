export const STUDENT_CLASS = [
    { value: "GI", years: 3 },
    { value: "AK", years: 3 },
    { value: "KGI", years: 3 },
    { value: "MARK", years: 3 },
    { value: "GS", years: 4 },
    { value: "MM", years: 3 },
    { value: "EM", years: 4 },
    { value: "KM", years: 4 },
    { value: "PR", years: 3 },
    { value: "ROA", years: 3 },
    { value: "VOA", years: 3 },
    { value: "VSZ", years: 2 },
]