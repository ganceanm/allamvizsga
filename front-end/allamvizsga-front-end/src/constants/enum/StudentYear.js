export const STUDENT_YEAR = [
    { value: "I", display: "I", hidden: 1 },
    { value: "II", display: "II", hidden: 2 },
    { value: "III", display: "III", hidden: 3 },
    { value: "IV", display: "IV", hidden: 4 }
]