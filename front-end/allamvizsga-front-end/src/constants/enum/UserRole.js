export const USER_ROLE = [
    { value: "STUDENT", display: "Hallgató" },
    { value: "ROOM_ADMIN", display: "Adminisztrátor" },
    { value: "ECONOMIC", display: "Gazdasági" },
    { value: "ADMIN", display: "Rendszergazda" },
]