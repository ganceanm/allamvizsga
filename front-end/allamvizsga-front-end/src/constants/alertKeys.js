const alertKeys = {
    login: {
        expired: {
            text: "Lejárt a bejelentkezés!",
            severity: "error"
        },
        passwordError: {
            text: "Rossz jelszó!",
            severity: "error"
        },
        userNameError: {
            text: "Rossz felhasználónév!",
            severity: "error"
        },
        userExists: {
            text: "Lejárt a bejelentkezés!",
            severity: "error"
        },
        registrationSuccess: {
            text: "Lejárt a bejelentkezés!",
            severity: "success"
        },
        resetPassSuccess: {
            text: "Ellenőrizd az e-mail fiókod! Nemsokára kapsz egy e-mailt benne a további lépésekkel a jelszavad visszaállításához!",
            severity: "success"
        },
        linkExpired: {
            text: "Ez a link már nem érvényes!",
            severity: "error"
        },
    },
    general: {
        default: {
            text: "Hiba!",
            severity: "error"
        },
        saveSuccess: {
            text: "Sikeres mentés!",
            severity: "success"
        }
    },
    users: {
        notuniqueemail: {
            text: "Ezzel az e-mail címmel már létezik regisztrált felhasználó!",
            severity: "error"
        }
    },
    room: {
        tooManyUsers: {
            text: "Hiba: túl sok felhasználó!",
            severity: "error"
        }
    },
    payment: {
        monthPaid: {
            text: "Ez a hónap már fizetve van!",
            severity: "error"
        },
        userError: {
            text: "Hiba: a felhasználó nem található!",
            severity: "error"
        }
    }
}

const getAlert = (path) => {
    if (!path) {
        return alertKeys.general.default
    }

    const items = path.split('.')

    const response = alertKeys[items[0]][items[1]]

    if (response) {
        return response;
    } else {
        return alertKeys.general.default
    }
}


export default getAlert;