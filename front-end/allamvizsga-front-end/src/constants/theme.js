import { createMuiTheme } from '@material-ui/core/styles';

export const DRAWER_WIDTH = 240;

export const APP_BAR_HEIGHT = 70;

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#0A6C43"
        },
        secondary: {
            main: '#0a646c',
        },
    },
});
