export const PAYMENT_TYPE = {
    ROOM: { value: "Room", display: "Szoba" },
    GENERAL: { value: "General", display: "Általános" }
}

export const MONTHS = {
    JAN: { value: 1, display: "Január" },
    FEB: { value: 2, display: "Február" },
    MAR: { value: 3, display: "Március" },
    APR: { value: 4, display: "Április" },
    MAY: { value: 5, display: "Május" },
    JUN: { value: 6, display: "Június" },
    JUL: { value: 7, display: "Július" },
    AUG: { value: 8, display: "Augusztus" },
    SEP: { value: 9, display: "Szeptember" },
    OCT: { value: 10, display: "Október" },
    NOV: { value: 11, display: "November" },
    DEC: { value: 12, display: "December" },
}